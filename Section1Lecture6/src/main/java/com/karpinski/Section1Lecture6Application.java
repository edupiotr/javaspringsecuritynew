package com.karpinski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Section1Lecture6Application {

	public static void main(String[] args) {
		SpringApplication.run(Section1Lecture6Application.class, args);
	}

}
