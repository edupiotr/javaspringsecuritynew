package com.karpinski.domain;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@Data
public class Authorities implements GrantedAuthority {
    private static final long serialVersionUID = -8123526131047887755L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    public Long id;

    public String authority;

//    @ManyToOne
//    @JoinColumn("user_id")

    @ManyToOne
    public User user;

//    @Override
//    public String getAuthority() {
//        return authority;
//    }
//
//    public void setAuthority(String authority) {
//        this.authority = authority;
//    }
//
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }

}
