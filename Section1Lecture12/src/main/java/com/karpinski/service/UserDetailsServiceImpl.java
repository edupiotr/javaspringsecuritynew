package com.karpinski.service;

import com.karpinski.com.karpinski.security.CustomSecurityUser;
import com.karpinski.domain.User;
import com.karpinski.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if(user == null) {
            throw new UsernameNotFoundException("Username and or password was incorrect.");
        }
        printUser(user);
        return new CustomSecurityUser(user);
    }

    private void printUser(User user) {
        System.out.println("Username = " + user.getUsername());
        System.out.println("Password = " + user.getPassword());
        System.out.println("Authorities = " + user.getAuthorities());
    }
}
