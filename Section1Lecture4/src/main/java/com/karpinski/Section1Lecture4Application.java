package com.karpinski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Section1Lecture4Application {

	public static void main(String[] args) {
		SpringApplication.run(Section1Lecture4Application.class, args);
	}

}
