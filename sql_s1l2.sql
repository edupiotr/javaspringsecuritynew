create database springsecuritydemo;

-- Lecture 8
-- dodanie usera
insert into user (username, password) values ('p.karpinski@gmail.com', '$2a$10$Hx9X/Nj8vOLnnWNu.yV8PeV7gh6chGq8EK3m1GB8auV3jsmh2C5VO');

-- dodanie roli dla usera
INSERT INTO authorities (authority, user_id) VALUES ('ROLE_USER', 1);
-- INSERT INTO authorities (authority, user_id) VALUES ('ROLE_ADMIN', 1);



select * from user;
select * from authorities;

delete from user where username='p.karpinski@gmail.com';


DROP TABLE authorities;
DROP TABLE user;
